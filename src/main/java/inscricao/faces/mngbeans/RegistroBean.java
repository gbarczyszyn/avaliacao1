/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import inscricao.entity.Login;
import java.util.ArrayList;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

/**
 *
 * @author guilherme
 */
@Named(value = "registroBean")
@ManagedBean
@SessionScoped
public class RegistroBean extends utfpr.faces.support.PageBean {

    private ArrayList<Login> loginsList;

    public ArrayList<Login> getLoginsList() {
        return loginsList;
    }

    public void setLoginsList(ArrayList<Login> loginsList) {
        this.loginsList = loginsList;
    }

    public RegistroBean() {
        loginsList = new ArrayList<Login>();
    }

    public void addLogin(Login login) {
        loginsList.add(login);
    }
}