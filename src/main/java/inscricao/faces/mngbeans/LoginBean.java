/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import inscricao.entity.Login;
import java.io.Serializable;
import java.util.Date;
import javax.annotation.ManagedBean;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import utfpr.faces.support.PageBean;

/**
 *
 * @author guilherme
 */
@Named("loginBean")
@SessionScoped
@ManagedBean
public class LoginBean extends PageBean implements Serializable {
    private String usuario;
    private String senha;
    private boolean administrador;
    private String resultado ="";
    private Date hora;
    
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public boolean isAdministrador() {
        return administrador;
    }

    public void setAdministrador(boolean administrador) {
        this.administrador = administrador;
    }
    
    public String loginAction() {
        if (usuario.equals(senha)) {
            //return true;
            resultado = "";
            hora = new Date();
            Login login = new Login();
            login.setHora(new Date());
            login.setUsuario(usuario);
            RegistroBean bean = (RegistroBean) getBean("registroBean");
            bean.addLogin(login);
            
            if (administrador){
                return "admin";
            }
            else{
                return "cadastro";
            }
        }
        resultado = "Acesso negado";
        return null;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public Date getHora() {
        return hora;
    }

    public void setHora(Date hora) {
        this.hora = hora;
    }
}
